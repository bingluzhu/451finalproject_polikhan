﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Go_Up_Down : MonoBehaviour
{


     public float frequency = 1f;    //movement speed
     public float amplitude = 1f;    //movement amount




     Vector3 startPos;
     float elapsedTime = 1f;



     // Use this for initialization
     void Start () {
         startPos = transform.position;
         float x = startPos.x;
         float y = startPos.y + Random.Range(-1, 3);
         float z = startPos.z;

         // Debug.Log(startPos);
         // transform.position = startPos + Vector3.up * Random.Range(1f, 20f);
         // transform.Translate(0,  Random.Range(1f, 20f), 0);
         Vector3 pos = new Vector3(x, y, z);
         startPos = pos;
         // startPos = transform.position;



     }
     // Update is called once per frame
     void Update () {
         elapsedTime += Time.deltaTime * Time.timeScale * frequency;
         transform.position = startPos + Vector3.up * Mathf.Sin(elapsedTime) * amplitude ;
     }

}
