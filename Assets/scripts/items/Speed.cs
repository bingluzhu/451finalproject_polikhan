﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Speed : MonoBehaviour
{

    Rigidbody rb;

    // Start is called before the first frame update
    void Start()
    {
      rb = GetComponent<Rigidbody>();

    }

    // Update is called once per frame
    void Update()
    {
      activate();
    }


    void activate(){
			Vector3 p1 = transform.position;
			// Debug.Log("p1" + p1);

			Collider[] hitColliders = Physics.OverlapBox(p1, transform.localScale / 2, Quaternion.identity);
			 int i = 0;
			 while (i < hitColliders.Length)
			 {
					 // Debug.Log("Hit : " + hitColliders[i].name + i);
					 GameObject playerToHit = hitColliders[i].gameObject;
					 Player player = playerToHit.GetComponent<Player>();
						if (player != null){
							player.speedUp();
              Destroy(this.gameObject,1);
              break;
						}
					 i++;
			 }
		}



}
