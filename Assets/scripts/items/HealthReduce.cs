﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealthReduce : MonoBehaviour
{
    float timer = 0;
    // set this up in the inspector!
    public float damageTime = 2;
    public int damageAmount = 15;
    void OnCollisionStay(Collision hit)
    {
        print("istriggered");
        if (hit.gameObject.tag == "Player")
        {
            print("stepped on");
            // Damage the player every 'damageTime'
            if (timer >= damageTime)
            {
                timer -= damageTime;
                // use the generic version of GetComponent, because it's faster
                PlayerHealth hp = hit.gameObject.GetComponent<PlayerHealth>();
                hp.TakeDamage(damageAmount);
            }
            timer += Time.deltaTime;
        }
    }
    void OnCollisionExit()
    {
        timer = 0;

    }
}
