﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealthPickUp : MonoBehaviour
{
    // Reference Variables //
    Rigidbody myRigidbody;

    // Start is called before the first frame update
    void Start()
    {
        myRigidbody = GetComponent<Rigidbody>();

    }

    // Update is called once per frame
    void Update()
    {
        
    }
    void OnCollisionEnter(Collision col)
    {
        print("on collision");
        if (col.gameObject.tag == "Player")
        {
            print("pick up health");
            var playerHealth = col.gameObject.GetComponent<PlayerHealth>();
            playerHealth.HealPlayer();
            Destroy(gameObject);
        }
    }
}
