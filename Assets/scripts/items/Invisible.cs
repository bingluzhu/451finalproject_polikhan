﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Invisible : MonoBehaviour
{



     public float frequency = 1f;    //movement speed
     public float amplitude = 2000f;    //movement amount
     Vector3 startPos;
     float elapsedTime = 1f;


     // Use this for initialization
     void setStartPos () {
         // startPos = transform.position;
         // float x = startPos.x;
         // float y = startPos.y + Random.Range(-1, 3);
         // float z = startPos.z;
         // Vector3 pos = new Vector3(x, y, z);
         startPos = transform.position;
     }


    Rigidbody rb;

    // Start is called before the first frame update
    void Start()
    {
      startPos = transform.position;
      rb = GetComponent<Rigidbody>();

    }

    void move(){
      elapsedTime += Time.deltaTime * Time.timeScale * frequency;
      transform.position = startPos + Vector3.up * Mathf.Sin(elapsedTime) * amplitude ;
      // Debug.Log(transform.position);
    }


    // Update is called once per frame
    void Update()
    {


      move ();
      activate();
    }


    void activate(){
			Vector3 p1 = startPos;
			// Debug.Log("p1" + p1);

			Collider[] hitColliders = Physics.OverlapBox(p1, transform.localScale / 2, Quaternion.identity);
			 int i = 0;
			 while (i < hitColliders.Length)
			 {
					 // Debug.Log("Hit : " + hitColliders[i].name + i);
					 GameObject playerToHit = hitColliders[i].gameObject;
					 Player player = playerToHit.GetComponent<Player>();
						if (player != null){
							player.becomeInvisible();
              Destroy(this.gameObject,1);
              return;
						}
					 i++;
			 }
		}



}
