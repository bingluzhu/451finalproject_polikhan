using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Flag : MonoBehaviour
{

  public int id;

  public GameObject interactionTarget;

  public void OnCollisionEnter(Collision other){

    Player targetPlayer = other.gameObject.GetComponent<Player>();

    if (targetPlayer != null && targetPlayer.playerID==id) {
      targetPlayer.captureFlag();
      Destroy(gameObject);
      interactionTarget.active = true;
    }


  }


  void update(){
  }






}