using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class WinningZone : MonoBehaviour
{

  public int id;

  public GameObject OtherPlayer;

  public void OnCollisionEnter(Collision other){

    Player targetPlayer = other.gameObject.GetComponent<Player>();

    if (targetPlayer != null && targetPlayer.playerID==id && targetPlayer.flagCaptured==true) {
      Debug.Log("Player won!");
      Destroy(gameObject);
      OtherPlayer.GetComponent<Player>().takeDamage(1000);
    }


  }


  void update(){
  }






}