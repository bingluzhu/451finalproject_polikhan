using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace SimpleHealthBar_finalProject{

public class Bomb	 : MonoBehaviour {
	Rigidbody rb;
	Animator m_Animator;
	CharacterController charCtrl;

	public float speed;
	public int hitpoints = 100;
	public float attackRange = 100;
	public bool active = true;

	// box cast
	Collider m_Collider;
	float m_MaxDistance;
	float m_Speed;
	bool m_HitDetect;
	RaycastHit m_Hit;
	public GameObject InstaBomb;



// create 2 lines of bomb
	void BombExpolde()
	{
			m_Animator.Play("attack01",-1,1);

			for(int i=0;i<5;i++)
			{
					Instantiate(InstaBomb, rb.transform.position+new Vector3(1, 0, i), Quaternion.identity);
					Instantiate(InstaBomb, rb.transform.position+new Vector3(i, 0, 1), Quaternion.identity);
					Instantiate(InstaBomb, rb.transform.position+new Vector3(1, 0, -i), Quaternion.identity);
					Instantiate(InstaBomb, rb.transform.position+new Vector3(-i, 0, 1), Quaternion.identity);
			}


			Destroy(this.gameObject,1);
	}



    // Use this for initialization
    void Start () {
				rb = GetComponent<Rigidbody>();
				m_Animator = gameObject.GetComponent<Animator>();
				// charCtrl = GetComponent<CharacterController>();

				// box cast
				m_Collider = GetComponent<Collider>();
				m_MaxDistance = 300.0f;
        m_Speed = 20.0f;

				Invoke("animateBoom", 4);
    }

		void animateBoom(){
			Debug.Log("boom");
			RaycastHit hit;
			Vector3 p1 = transform.position;

			Debug.Log("p1" + p1);

			// GameObject playerToHit = hit.transform.gameObject;
			// Player player = playerToHit.GetComponent<Player>();
			// Collider[] hitColliders = Physics.OverlapSphere(p1, attackRange);
			// if (hitColliders.Length != 0){
			//   Debug.Log("Found something!");
			// }
		  // Debug.Log(transform.TransformDirection);


			m_HitDetect = Physics.BoxCast(m_Collider.bounds.center, transform.localScale, transform.forward, out m_Hit, transform.rotation, m_MaxDistance);
	        if (m_HitDetect){
	            //Output the name of the Collider your Box hit
	            Debug.Log("Hit : " + m_Hit.collider.name);
							GameObject playerToHit = m_Hit.collider.gameObject;
							Player player = playerToHit.GetComponent<Player>();
							if (player != null){
								player.takeDamage(50);
								// Debug.Log("player health" +player.currentHealth);
							}
	        }


			// for moving items only
			// charCtrl = GetComponent<CharacterController>();
			// float distanceToObstacle = 0;
			// if (Physics.SphereCast(p1, 10, transform.forward, out hit, 10)){
			// 			if (player != null){
			// 				Debug.Log("hit0" + hit.distance);
			// 			}
			//
      //       distanceToObstacle = hit.distance;
      // }



			m_Animator.Play("attack01");
			this.active = false;
			// Destroy(this.gameObject,1);
		}

    /*
	void FixedUpdate(){
		float moveHorizontal = Input.GetAxis("Horizontal");
		float moveVertical = Input.GetAxis("Vertical");
		Vector3 movement = new Vector3(moveHorizontal, 0, moveVertical);
		// console.log(Vector3);

		if(Camera.main!=null){
			Vector3 cameraForward = Vector3.Scale(Camera.main.transform.forward, new Vector3(1,0,1)).normalized;
			movement = moveVertical* cameraForward + moveHorizontal * Camera.main.transform.right;
		}
		rb.AddForce(movement * speed);

	}*/

	IEnumerator boom(Player player){
		yield return new WaitForSeconds (2);
		player.takeDamage(hitpoints);
	}





	// Update is called once per frame
	void Update () {
		if (this.active){
			m_Animator.Play("damage");
		}
		// m_Animator.Play("idle");

        //if (Input.GetKeyDown(KeyCode.A))
        //{
        //    //Debug.Log("Space key was pressed.");
        //    m_Animator.Play("attack01",-1,1);

        //    Destroy(this.gameObject,1);
        //}

    }
}
}
