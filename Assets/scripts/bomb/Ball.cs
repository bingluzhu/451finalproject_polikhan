using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// namespace SimpleHealthBar_finalProject{

public class Ball	 : MonoBehaviour {
		public Rigidbody rb;

		public float speed;
    public Animator m_Animator;

		float m_MaxDistance = 3000.0f;
		public Collider m_Collider;

		public int hitpoints = 100;
		public bool m_HitDetect;

		public RaycastHit m_Hit;



    //Bomb Prefab
    public GameObject InstaBomb;

    //Tiles length

    // Use this for initialization
    void Start () {
			Debug.Log("bomb created");

			rb = GetComponent<Rigidbody>();
	    m_Animator = gameObject.GetComponent<Animator>();
			m_Collider = GetComponent<Collider>();
      Invoke("BombExpolde",2);
    }


    void BombExpolde()
    {
				animateBoom();
				// m_Animator.Play("attack01",-1,1);

        for(int i=0;i<5;i++)
        {
            Instantiate(InstaBomb, rb.transform.position+new Vector3(1, 0, i), Quaternion.identity);
            Instantiate(InstaBomb, rb.transform.position+new Vector3(i, 0, 1), Quaternion.identity);
            Instantiate(InstaBomb, rb.transform.position+new Vector3(1, 0, -i), Quaternion.identity);
            Instantiate(InstaBomb, rb.transform.position+new Vector3(-i, 0, 1), Quaternion.identity);
        }


        // Destroy(this.gameObject,1);
    }

    void CreateExplosion()
    {

    }
    /*
    int getExplosionRange()
    {

    }*/

    /*
	void FixedUpdate(){
		float moveHorizontal = Input.GetAxis("Horizontal");
		float moveVertical = Input.GetAxis("Vertical");
		Vector3 movement = new Vector3(moveHorizontal, 0, moveVertical);
		// console.log(Vector3);

		if(Camera.main!=null){
			Vector3 cameraForward = Vector3.Scale(Camera.main.transform.forward, new Vector3(1,0,1)).normalized;
			movement = moveVertical* cameraForward + moveHorizontal * Camera.main.transform.right;
		}
		rb.AddForce(movement * speed);

	}*/





		// option1 box cast
		// void animateBoom(){
		// 	Debug.Log("boom");
		// 	Vector3 p1 = transform.position;
		// 	Debug.Log("p1" + p1);
		// 	// LayerMask layerMask = 1 << 8;
		//
		// 	m_HitDetect = Physics.BoxCast(m_Collider.bounds.center, transform.localScale, transform.forward, out m_Hit, transform.rotation, m_MaxDistance);
		//
		// 	// m_HitDetect = Physics.CheckBox(p1, transform.localScale*2, Quaternion.identity);
	  //       if (m_HitDetect){
	  //           //Output the name of the Collider your Box hit
	  //           Debug.Log("Hit : " + m_Hit.collider.name);
		// 					GameObject playerToHit = m_Hit.collider.gameObject;
		// 					Player player = playerToHit.GetComponent<Player>();
		// 					if (player != null){
		// 						player.takeDamage(50);
		// 						Debug.Log("player health" +player.currentHealth);
		// 					}
	  //       }
		// 	m_Animator.Play("attack01");
		// 	// this.active = false;
		// 	Destroy(this.gameObject,1);
		// }



// option 2, simply check collider
		void animateBoom(){
			Debug.Log("boom");
			Vector3 p1 = transform.position;
			Debug.Log("p1" + p1);

			Collider[] hitColliders = Physics.OverlapBox(p1, transform.localScale / 2, Quaternion.identity);
			 int i = 0;
			 //Check when there is a new collider coming into contact with the box

			 while (i < hitColliders.Length)
			 {
					 // Debug.Log("Hit : " + hitColliders[i].name + i);
					 GameObject playerToHit = hitColliders[i].gameObject;
					 Player player = playerToHit.GetComponent<Player>();
						if (player != null){
							player.takeDamage(50);
							// Debug.Log("player health" +player.currentHealth);
							break;
						}
					 i++;
			 }


			m_Animator.Play("attack01");
			// this.active = false;
			Destroy(this.gameObject,1);
		}





	// Update is called once per frame
	void Update () {
        //if (Input.GetKeyDown(KeyCode.A))
        //{
        //    //Debug.Log("Space key was pressed.");
        //    m_Animator.Play("attack01",-1,1);

        //    Destroy(this.gameObject,1);
        //}

    }

	}
// }
