﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
// namespace SimpleHealthBar_finalProject{

  public class PlayerHealth : MonoBehaviour
  {
      // public static PlayerHealth instance;
      // public static PlayerHealth Instance { get { return instance; } }


      public PlayerHealth instance;
      // public static PlayerHealth Instance { get { return instance; } }


      bool canTakeDamage = true;

      public float maxHealth = 1000;
      public float currentHealth;

      public SimpleHealthBar healthBar;


      void Awake()
      {
          // If the instance variable is already assigned, then there are multiple player health scripts in the scene. Inform the user.
          if (instance != null)
              Debug.LogError("There are multiple instances of the Player Health script. Assigning the most recent one to Instance.");

          // Assign the instance variable as the Player Health script on this object.
          instance = GetComponent<PlayerHealth>();
      }

      void Start()
      {
          // Set the current health and shield to max values.
          currentHealth = maxHealth;

          // Update the Simple Health Bar with the updated values of Health and Shield.
          healthBar.UpdateBar(currentHealth, maxHealth);
      }

      void Update()
      {
      }

      public void HealPlayer()
      {
          // Increase the current health by 25%.
          currentHealth += (maxHealth / 4);

          // If the current health is greater than max, then set it to max.
          if (currentHealth > maxHealth)
              currentHealth = maxHealth;

          // Update the Simple Health Bar with the new Health values.
          healthBar.UpdateBar(currentHealth, maxHealth);
      }

      public void TakeDamage(float damage)
      {

          Debug.Log("player health script");
          // If the player can't take damage, then return.
          // if (canTakeDamage == false)
          //     return;
          currentHealth -= damage;

          // If the health is less than zero...
          if (currentHealth <= 0)
          {
              // Set the current health to zero.
              currentHealth = 0;

              // Run the Death function since the player has died.
              Death();
          }

          // Set canTakeDamage to false to make sure that the player cannot take damage for a brief moment.
          // canTakeDamage = false;

          // Update the Health and Shield status bars.
          healthBar.UpdateBar(currentHealth, maxHealth);
      }

      public void Death()
      {
          // Show the death screen, and disable the player's control.

      }

  // }
}
