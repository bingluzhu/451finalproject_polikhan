using System.Collections;
using System.Collections.Generic;
using UnityEngine;
// namespace UnityStandardAssets.Characters.ThirdPerson
// {
// 	using static UnityStandardAssets.Characters.ThirdPerson.ThirdPersonCharacter;
//
// }
// namespace SimpleHealthBar_finalProject{
	public class Player : MonoBehaviour {


		public UnityStandardAssets.Characters.ThirdPerson.ThirdPersonCharacter characterControlScript;

		public PlayerHealth playerHealth;

		// Rigidbody rb;
		//prefab: bomb
	  // public GameObject bomb;
		// public int startingHealth = 100;
		// public int currentHealth = 100;
		// public Slider healthSlider;
		// public bool playerAlive = true;

		public bool isInvisilbe = false;



		public float speed;
		public bool grounded = true;
		public float jumpPower = 0;
		public bool isDead = false;
		public int playerID;


		public bool flagCaptured=false;

		// Use this for initialization
		void Start () {
			// rb = GetComponent<Rigidbody>();
			// console.log("start");
			playerHealth = this.gameObject.GetComponent<PlayerHealth>();

			// load Characters script
			characterControlScript =  this.gameObject.GetComponent<UnityStandardAssets.Characters.ThirdPerson.ThirdPersonCharacter>();

		}



		void Awake(){
			// this.currentHealth = this.startingHealth;
		}



		// void CreateBomb()
		// {
		//
		//
	  //       Instantiate(bomb, rb.transform.position, Quaternion.identity);
		//
		//
		// }



		/*
		void FixedUpdate(){
			float moveHorizontal = Input.GetAxis("Horizontal");
			float moveVertical = Input.GetAxis("Vertical");
			Vector3 movement = new Vector3(moveHorizontal, 0, moveVertical);
			// console.log(Vector3);

			if(Camera.main!=null){
				Vector3 cameraForward = Vector3.Scale(Camera.main.transform.forward, new Vector3(1,0,1)).normalized;
				movement = moveVertical* cameraForward + moveHorizontal * Camera.main.transform.right;
			}
			rb.AddForce(movement * speed);

		}*/

		// Update is called once per frame
		void Update () {
        // 	if (Input.GetKeyDown(KeyCode.B)) {
        // 	// Invoke("CreateBomb",2);
        // 		CreateBomb();
        //   	}
        //
        //
        //   if(!grounded && rb.velocity.y == 0) {
        // 		grounded = true;
        // 	}
        // 	/*
        // 	if (Input.GetKeyDown(KeyCode.X) && grounded == true) {
        // 		   print("x key was pressed");
        // 			 rb.AddForce(transform.up*jumpPower,  ForceMode.Impulse);
        // 			 grounded = false;
        // 	}*/
            Vector3 checkPoint = new Vector3(transform.position.x, transform.position.y, transform.position.z);
            bool hitLava = Physics.Raycast(checkPoint, Vector3.down, Mathf.Infinity, LayerMask.GetMask("Lava"));
            if (hitLava)
            {
                print("hit lava");
                playerHealth.TakeDamage(0.5f);
            }
        }



		public void takeDamage(int damage) {
			if (this.isInvisilbe) return;
			playerHealth.TakeDamage(damage);

			// this.currentHealth -= damage;
			//
			// if (this.currentHealth >= 0)
			// {
			// }
			// else{
			// 	playerAlive = false;
			//
			// }

		}

		public void speedUp(){
			Debug.Log("speedUp");
			characterControlScript.speedUp();
			Invoke("speedBackToNormal", 10);

		}


		public void speedBackToNormal(){
			characterControlScript.speedBackToNormal();
		}

		public void becomeNotInvisible(){
			Debug.Log("Not Invisible");

			this.isInvisilbe = false;
		}

		public void becomeInvisible(){
			Debug.Log("hey IMA Invisible!");
			this.isInvisilbe = true;
			Invoke("becomeNotInvisible", 10);
		}

		public void captureFlag()
		{
			Debug.Log("flag captured!");
			this.flagCaptured=true;
		}


		void Death ()
		    {
		        // Set the death flag so this function won't be called again.
		        isDead = true;

		        // Turn off any remaining shooting effects.
		        // playerShooting.DisableEffects ();

		        // Tell the animator that the player is dead.
		        // anim.SetTrigger ("Die");

		        // Set the audiosource to play the death clip and play it (this will stop the hurt sound from playing).
		        // playerAudio.clip = deathClip;
		        // playerAudio.Play ();

		        // Turn off the movement and shooting scripts.
		        // playerMovement.enabled = false;
		        // playerShooting.enabled = false;
		    }



	}
// }
